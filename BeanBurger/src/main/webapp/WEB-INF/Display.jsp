<%-- 
    Document   : Display
    Created on : 2019.07.28., 16:53:32
    Author     : rajnaig
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hamburger főoldal</title>
    </head>
    <body>
        <p><input type button href="/OrderServlet" name="Cart"></p>
        <table>
        <tr>
            <td>
              <c:choose>
             <c:when test="${hamburger1Quantity==true}"> 
             <p name="hamburger1">${hamburger1.getName()}</p>
             <p name="hamburger1">${hamburger1.getPrice()}</p>
             <p name="hamburger1">${hamburger1.getQuantity()}</p>
             <input type="text" name="hamburger1" value="0"  min="0" max="${hamburger1.getQuantity()}">
             <input type="button" value="submit">Mehet</button>
             </c:when>
             <c:otherwise>
                 <p>Elfogyott</p>
                 </c:otherwise>
              </c:choose>   
            </td>
        </tr>
        <tr>
            <td>
             <c:choose>
             <c:when test="${hamburger2Quantity==true}"> 
             <p name="hamburger2">${hamburger2.getName()}</p>
             <p name="hamburger2">${hamburger2.getPrice()}</p>
             <p name="hamburger2">${hamburger2.getQuantity()}</p>
             <input type="text" name="hamburger2" value="0"  min="0" max="${hamburger2.getQuantity()}">
             <input type="button" value="submit">Mehet</button>
            </c:when>
             <c:otherwise>
                 <p>Elfogyott</p>
                 </c:otherwise>
              </c:choose>   
             </td>    
        </tr>
        <tr>
            <td>
                <c:choose>
             <c:when test="${hamburger3Quantity==true}"> 
                <p name="hamburger3">${hamburger3.getName()}</p>
             <p name="hamburger3">${hamburger3.getPrice()}</p>
             <p name="hamburger3">${hamburger3.getQuantity()}</p>
             <input type="text" name="hamburger3" value="0"  min="0" max="${hamburger3.getQuantity()}">
                <input type="button" value="submit">
                </c:when>
             <c:otherwise>
                 <p>Elfogyott</p>
                 </c:otherwise>
              </c:choose>   
                </td>
        </tr>
        <tr>
            <td>
                <c:choose>
             <c:when test="${hamburger4Quantity==true}"> 
             <p name="hamburger4">${hamburger4.getName()}</p>
             <p name="hamburger4">${hamburger4.getPrice()}</p>
             <p name="hamburger4">${hamburger4.getQuantity()}</p>
             <input type="text" name="hamburger4" value="0"  min="0" max="${hamburger4.getQuantity()}">
                <input type="button" value="submit">
                 </c:when>
             <c:otherwise>
                 <p>Elfogyott</p>
                 </c:otherwise>
              </c:choose> 
            </td>
        </tr>
        <tr>
            <td>
                   <c:choose>
             <c:when test="${hamburger5Quantity==true}"> 
             <p name="hamburger1">${hamburger5.getName()}</p>
             <p name="hamburger1">${hamburger5.getPrice()}</p>
             <p name="hamburger1">${hamburger5.getQuantity()}</p>
             <input type="text" name="hamburger5" value="0"  min="0" max="${hamburger5.getQuantity()}">
                <input type="button" value="submit">
                 </c:when>
             <c:otherwise>
                 <p>Elfogyott</p>
                 </c:otherwise>
              </c:choose> 
            </td>
        </tr>
        </table>    
    </body>
</html>
