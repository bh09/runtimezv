package hu.braininghub.beanburger.service;

import hu.braininghub.beanburger.dto.BurgerDTO;
import hu.braininghub.beanburger.entity.Burger;
import hu.braininghub.beanburger.repository.BurgerRepository;
import hu.braininghub.beanburger.util.Mapper;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class BurgerService {

    @EJB
    private BurgerRepository br;

    public void saveBurger(BurgerDTO burger) {
        br.saveEntity(Mapper.mapBurgerDtoToEnyit(burger));
    }

    public List<BurgerDTO> getAllBUrgers() {

        return Mapper.mapEntitysListToDtosList(br.getAllBUrger());
    }

}
