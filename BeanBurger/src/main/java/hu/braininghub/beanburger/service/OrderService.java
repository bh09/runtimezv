package hu.braininghub.beanburger.service;

import hu.braininghub.beanburger.entity.Order;
import hu.braininghub.beanburger.repository.OrderRepository;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class OrderService {
    
    @EJB
    private OrderRepository or;
    
    public void saveOrders(Order order){
    
        or.saveOrder(order);
    }

}
