package hu.braininghub.beanburger.repository;

import hu.braininghub.beanburger.entity.Order;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Stateless
@LocalBean
public class OrderRepository {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void saveOrder(Order ord) {
        em.persist(ord);
    }

}
