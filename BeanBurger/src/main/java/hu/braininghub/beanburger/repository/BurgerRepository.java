package hu.braininghub.beanburger.repository;

import hu.braininghub.beanburger.entity.Burger;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

@Stateless
@LocalBean
public class BurgerRepository {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void saveEntity(Burger burger) {
        em.persist(burger);
    }

    public List<Burger> getAllBUrger() {
        Query q = em.createQuery("from Burger");
        return q.getResultList();
    }

}
