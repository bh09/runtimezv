package hu.braininghub.beanburger.util;

import hu.braininghub.beanburger.dto.BurgerDTO;
import hu.braininghub.beanburger.dto.OrderDTO;
import hu.braininghub.beanburger.entity.Burger;
import hu.braininghub.beanburger.entity.BurgerType;
import hu.braininghub.beanburger.entity.Order;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.beanutils.BeanUtils;

public class Mapper {

    public static BurgerDTO mapBurgerEntityToDto(Burger buger) {
        BurgerDTO bdto = new BurgerDTO();
        try {

            BeanUtils.copyProperties(bdto, buger);
            bdto.setBurgerType(buger.getBurgerType().toString());

            //return valami
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(Mapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bdto;
    }

    public static Burger mapBurgerDtoToEnyit(BurgerDTO bdto) {
        Burger burger = new Burger();
        try {

            BeanUtils.copyProperties(burger, bdto);
            burger.setBurgerType(BurgerType.valueOf(bdto.getBurgerType()));

        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(Mapper.class.getName()).log(Level.SEVERE, null, ex);
        }

        return burger;
    }

    public static OrderDTO mapOrderEntityToDto(Order order) {
        OrderDTO odto = new OrderDTO();

        try {

            BeanUtils.copyProperties(order, odto);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(Mapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        return odto;

    }

    public static List<BurgerDTO> mapEntitysListToDtosList(List<Burger> burerList) {
        List<BurgerDTO> burgerDtoList = new ArrayList<>();
        burerList.forEach(p -> {
            BurgerDTO bdto = new BurgerDTO();
            try {
                BeanUtils.copyProperties(bdto, p);
                bdto.setBurgerType(p.getBurgerType().toString());
                burgerDtoList.add(bdto);
            } catch (IllegalAccessException | InvocationTargetException ex) {
                Logger.getLogger(Mapper.class.getName()).log(Level.SEVERE, null, ex);
            }

        });
        burgerDtoList.sort((o1, o2) -> o1.getBurgerType().compareTo(o2.getBurgerType()));
        return burgerDtoList;

    }

//    public static Order mapOrderDtoToEntity(OrderDTO orderdto) {
//
//    }
}
