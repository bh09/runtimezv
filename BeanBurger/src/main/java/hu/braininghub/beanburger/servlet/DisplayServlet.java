/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.beanburger.servlet;

import hu.braininghub.beanburger.dto.BurgerDTO;
import hu.braininghub.beanburger.service.BurgerService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author rajnaig
 */
@WebServlet(name = "DisplayServlet", urlPatterns = {"/DisplayServlet"})
public class DisplayServlet extends HttpServlet {
@EJB
BurgerService burgerService;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<BurgerDTO> hamburgers=burgerService.getAllBUrgers();
        
        if(hamburgers.get(0).getCount()>0){
        request.setAttribute("hamburger1Quantity", true);
        request.setAttribute("hamburger1", hamburgers.get(0));
        }else{
        request.setAttribute("hamburger1Quantity", false);    
        }
        if(hamburgers.get(1).getCount()>0){
            request.setAttribute("hamburger2Quantity", true);
        request.setAttribute("hamburger2", hamburgers.get(1));
        }else{
            request.setAttribute("hamburger2Quantity", false);
        }
        if(hamburgers.get(2).getCount()>0){
            request.setAttribute("hamburger3Quantity", true);
        request.setAttribute("hamburger3", hamburgers.get(2));
        }else{
            request.setAttribute("hamburger3Quantity", false);
        }
        if(hamburgers.get(3).getCount()>0){
        request.setAttribute("hamburger4", hamburgers.get(3));
         request.setAttribute("hamburger4Quantity", true);
        }else{
          
            request.setAttribute("hamburger4Quantity", false);
        }
        if(hamburgers.get(4).getCount()>0){
        request.setAttribute("hamburger5", hamburgers.get(4));
        request.setAttribute("hamburger5Quantity", true);
        }else{
        request.setAttribute("hamburger5Quantity", false);    
        }
     request.getRequestDispatcher("/WEB-INF/Display.jsp").forward(request,response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
     List<BurgerDTO> hamburgers=burgerService.getAllBUrgers();
        if(hamburgers.get(0).getCount()>0){
        request.setAttribute("hamburger1Quantity", true);
        request.setAttribute("hamburger1", hamburgers.get(0));
        }else{
        request.setAttribute("hamburger1Quantity", false);    
        }
        if(hamburgers.get(1).getCount()>0){
            request.setAttribute("hamburger2Quantity", true);
        request.setAttribute("hamburger2", hamburgers.get(1));
        }else{
            request.setAttribute("hamburger2Quantity", false);
        }
        if(hamburgers.get(2).getCount()>0){
            request.setAttribute("hamburger3Quantity", true);
        request.setAttribute("hamburger3", hamburgers.get(2));
        }else{
            request.setAttribute("hamburger3Quantity", false);
        }
        if(hamburgers.get(3).getCount()>0){
        request.setAttribute("hamburger4", hamburgers.get(3));
         request.setAttribute("hamburger4Quantity", true);
        }else{
          
            request.setAttribute("hamburger4Quantity", false);
        }
        if(hamburgers.get(4).getCount()>0){
        request.setAttribute("hamburger5", hamburgers.get(4));
        request.setAttribute("hamburger5Quantity", true);
        }else{
        request.setAttribute("hamburger5Quantity", false);    
        }   
    request.getRequestDispatcher("/WEB-INF/Display.jsp").forward(request,response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
