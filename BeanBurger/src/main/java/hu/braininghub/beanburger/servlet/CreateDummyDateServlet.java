/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.beanburger.servlet;

import hu.braininghub.beanburger.dto.BurgerDTO;
import hu.braininghub.beanburger.entity.Burger;
import hu.braininghub.beanburger.entity.BurgerType;
import hu.braininghub.beanburger.service.BurgerService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Dell7720
 */
@WebServlet(name = "CreateDummyDateServlet", urlPatterns = {"/CreateDummyDateServlet"})
public class CreateDummyDateServlet extends HttpServlet {

    @EJB
    BurgerService burgerService;
    
    @EJB
    Burger burger;

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        BurgerDTO b =  new BurgerDTO();
       b.setBurgerType("BEAN_BURGER");
       b.setCount(10);
       b.setPrice(300);
       burgerService.saveBurger(b);
       BurgerDTO b2 =  new BurgerDTO();
       b.setBurgerType("LOMBOK_BURGER");
       b.setCount(10);
       b.setPrice(400);
       burgerService.saveBurger(b2);
       BurgerDTO b3 =  new BurgerDTO();
       b.setBurgerType("CHEESE_BURGER");
       b.setCount(10);
       b.setPrice(500);
       burgerService.saveBurger(b3);
       BurgerDTO b1 =  new BurgerDTO();
       b.setBurgerType("JAVA_BURGER");
       b.setCount(10);
       b.setPrice(600);
       burgerService.saveBurger(b1);
       BurgerDTO b4 =  new BurgerDTO();
       b.setBurgerType("SPRING_BURGER");
       b.setCount(10);
       b.setPrice(600);
       burgerService.saveBurger(b4);

        
        
        
        
        
        
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
