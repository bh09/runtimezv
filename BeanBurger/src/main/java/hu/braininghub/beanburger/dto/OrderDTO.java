
package hu.braininghub.beanburger.dto;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author Greg Takacs
 */
public class OrderDTO {
    
    private Long id;
    private String email;
    private List<BurgerDTO> burgerList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<BurgerDTO> getBurgerList() {
        return burgerList;
    }

    public void setBurgerList(List<BurgerDTO> burgerList) {
        this.burgerList = burgerList;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        hash = 79 * hash + Objects.hashCode(this.email);
        hash = 79 * hash + Objects.hashCode(this.burgerList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrderDTO other = (OrderDTO) obj;
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.burgerList, other.burgerList)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "OrderDTO{" + "id=" + id + ", email=" + email + ", burgerList=" + burgerList + '}';
    }
    
    

}
