package hu.braininghub.beanburger.dto;

import hu.braininghub.beanburger.entity.BurgerType;
import java.util.Objects;

/**
 *
 * @author Greg Takacs
 */
public class BurgerDTO {

    private Long id;
    private Integer price;
    private Integer count;
    private String burgerType;

    public BurgerDTO() {
    }

    public BurgerDTO(Long id, Integer price, Integer count, String burgerType) {
        this.id = id;
        this.price = price;
        this.count = count;
        this.burgerType = burgerType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getBurgerType() {
        return burgerType;
    }

    public void setBurgerType(String burgerType) {
        this.burgerType = burgerType;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.id);
        hash = 37 * hash + Objects.hashCode(this.price);
        hash = 37 * hash + Objects.hashCode(this.count);
        hash = 37 * hash + Objects.hashCode(this.burgerType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BurgerDTO other = (BurgerDTO) obj;
        if (!Objects.equals(this.burgerType, other.burgerType)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.price, other.price)) {
            return false;
        }
        if (!Objects.equals(this.count, other.count)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BurgerDTO{" + "id=" + id + ", price=" + price + ", count=" + count + ", burgerType=" + burgerType + '}';
    }

}
