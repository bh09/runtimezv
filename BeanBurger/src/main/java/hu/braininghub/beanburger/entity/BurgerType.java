
package hu.braininghub.beanburger.entity;

/**
 *
 * @author Greg Takacs
 */
public enum BurgerType {
    
    BEAN_BURGER,
    LOMBOK_BURGER,
    CHEESE_BURGER,
    JAVA_BURGER,
    SPRING_BURGER;

}
