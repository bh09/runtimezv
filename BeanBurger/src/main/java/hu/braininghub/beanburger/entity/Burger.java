
package hu.braininghub.beanburger.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Greg Takacs
 */
@Entity
public class Burger implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Integer price;
    private Integer count;
    private BurgerType burgerType;

    public Burger() {
    }

    public Burger(Integer price, Integer count, BurgerType burgerType) {
        this.price = price;
        this.count = count;
        this.burgerType = burgerType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BurgerType getBurgerType() {
        return burgerType;
    }

    public void setBurgerType(BurgerType burgerType) {
        this.burgerType = burgerType;
    }
    
    
    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.id);
        hash = 67 * hash + Objects.hashCode(this.price);
        hash = 67 * hash + Objects.hashCode(this.count);
        hash = 67 * hash + Objects.hashCode(this.burgerType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Burger other = (Burger) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.price, other.price)) {
            return false;
        }
        if (!Objects.equals(this.count, other.count)) {
            return false;
        }
        if (this.burgerType != other.burgerType) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Burger{" + "id=" + id + ", price=" + price + ", count=" + count + ", burgerType=" + burgerType + '}';
    }
    
    
    

}
